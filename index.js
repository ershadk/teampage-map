const fs = require('fs');

const _ = require('lodash');
const Promise = require('bluebird');

const axios = require('axios');
const querystring = require('qs');
const states = require('us-state-codes');
const yaml = require('yaml-js');

const TEAM_MAP_VERSION = 7;

if (!process.env.GEONAMES_USERNAME) {
  throw new Error('env variable GEONAMES_USERNAME must be set');
}

function mapCountriesToISO(country) {
  switch (country) {
    case 'Ukraine':
      return 'UA';
    case 'USA':
    case 'US':
    case 'U.S.A.':
    case 'United States':
    case 'Uninted States':
      return 'US';
    case 'Netherlands':
    case 'The Netherlands':
      return 'NL';
    case 'Portugal':
      return 'PT';
    case 'Poland':
      return 'PL';
    case 'United Kingdom':
    case 'UK':
      return 'GB';
    case 'Peru':
      return 'PE';
    case 'Brazil':
      return 'BR';
    case 'France':
      return 'FR';
    case 'Spain':
      return 'ES';
    case 'Germany':
      return 'DE';
    case 'Canada':
      return 'CA';
    case 'Sweden':
      return 'SE';
    case 'Israel':
      return 'IL';
    case 'Taiwan':
      return 'TW';
    case 'Chile':
      return 'CL';
    case 'Australia':
      return 'AU';
    case 'Nigeria':
      return 'NG';
    case 'India':
      return 'IN';
    case 'Malaysia':
      return 'MY';
    case 'Mexico':
      return 'MX';
    case 'Serbia':
      return 'RS';
    case 'South Africa':
      return 'ZA';
    case 'Belgium':
      return 'BE';
    case 'Czech Republic':
      return 'CZ';
    case 'China':
      return 'CN';
    case 'Ireland':
      return 'IE';
    case 'Austria':
      return 'AT';
    case 'Italy':
      return 'IT';
    case 'Zimbabwe':
      return 'ZW';
    case 'Japan':
      return 'JP';
    case 'Slovenia':
      return 'SI';
    case 'Egypt':
      return 'EG';
    case 'Nicaragua':
      return 'NI';
    case 'Russia':
      return 'RU';
    case 'Slovakia':
      return 'SK';
    case 'Greece':
      return 'GR';
    case 'Hungary':
      return 'HU';
    case 'Lithuania':
      return 'LT';
    case 'Malta':
      return 'MT';
    case 'New Zealand':
      return 'NZ';
    case 'Philippines':
      return 'PH';
    case 'Bosnia and Herzegovina':
      return 'BA';
    case 'Kenya':
      return 'KE';
    case 'Pakistan':
      return 'PK';
    case 'Luxembourg':
      return 'LU';
    case 'Mongolia':
      return 'MN';
    case 'Denmark':
      return 'DK';
    case 'Belarus':
      return 'BY';
    case 'Norway':
      return 'NO';
    case 'Argentina':
      return 'AR';
    case 'Moldova':
      return 'MD';
    case 'Dominican Republic':
      return 'DO';
    case 'Ecuador':
      return 'EC';
    case 'Singapore':
      return 'SG';
    case 'Romania':
      return 'RO';
    case 'Andorra':
      return 'AD';
    default:
      throw new Error(`Unknown country:
      case '${country}':
        return 'xxx';
      `);
  }
}

const geoNames = endpoint => (params = {}) => {
  return axios({
    method: 'get',
    url: `https://secure.geonames.org/${endpoint}`,
    params: {
      type: 'json',
      username: process.env.GEONAMES_USERNAME,
      ...params,
    },
    paramsSerializer: function(params) {
      return querystring.stringify(params, { arrayFormat: 'repeat' });
    },
  }).then(response => {
    const status = _.get(response, 'data.status', false);
    if (status) {
      throw new Error(
        `GeoNames raised error ${status.value}: ${status.message}. Search: ${response.request.path}`
      );
    }

    return _.get(response, 'data.geonames');
  });
};

const searchBlacklist = ['St. ', 'San '];

const startsWithBlackList = name => {
  if (!name) {
    return false;
  }

  if (searchBlacklist.some(b => name.startsWith(b))) {
    return true;
  }

  return !/^[\S]+$/.test(name.substr(0, 3));
};

const searchGeoNames = geoNames('search');
const countryInfo = geoNames('countryInfo');

const STATE_CODE_REGEX = /,\s*([A-Z]+)\s*$/i;

function getLocality(locationName) {
  let search = (locationName && locationName.trim()) || '';

  search = search.slice(0, 1).toLocaleUpperCase() + search.slice(1);

  return search !== 'Anywhere' ? search : '';
}

function getSearchQuery(locationName, country) {
  const search = locationName || false;
  const countryCode = mapCountriesToISO(country);

  if (search) {
    let options = {
      maxRows: 1,
      inclBbox: true,
      featureClass: ['P', 'A'],
      country: countryCode,
      orderby: 'relevance',
      q: search,
      isNameRequired: true,
    };

    if (countryCode === 'US' && STATE_CODE_REGEX.test(search)) {
      let state = search.match(STATE_CODE_REGEX)[1];
      options.q = search.replace(STATE_CODE_REGEX, '');

      if (state.length !== 2) {
        state = states.getStateCodeByStateName(states.sanitizeStateName(state));
      }

      if (state.length === 2) {
        options.adminCode1 = state.toUpperCase();
      }
    }

    //Ensure that abbreviations / spaces are not used for startsWith
    if (!startsWithBlackList(search)) {
      options.name_startsWith = search.substr(0, 3);
    }

    return searchGeoNames(options);
  }

  return countryInfo({ country: countryCode }).then(response => {
    const result = _.get(response, '[0]', {});

    const lng =
      result.east && result.west
        ? (parseFloat(result.east) + parseFloat(result.west)) / 2
        : undefined;

    const lat =
      result.north && result.south
        ? (parseFloat(result.north) + parseFloat(result.south)) / 2
        : undefined;

    return [{ ...result, lat, lng }];
  });
}

const funnyPlaceMap = {
  'Phila., PA': 'Philadelphia, PA',
  'Detroit Metro Area, MI': 'Detroit, MI',
  'Twin Cities, Minnesota': 'Minneapolis, Minnesota',
};

const cachedLocations = {};

const roundNumber = number => +parseFloat(number).toFixed(3);

function getLocation(locationName, country) {
  const key = `${locationName}|${country}`;

  if (cachedLocations[key]) {
    console.log(`Using cached coordinates for ${locationName} in ${country}`);
    return Promise.resolve(cachedLocations[key]);
  }

  if (funnyPlaceMap[locationName]) {
    locationName = funnyPlaceMap[locationName];
  }

  return getSearchQuery(locationName, country).then(response => {
    const result = _.get(response, '[0]', {});

    if (!result.lng || !result.lat) {
      throw new Error(`Could not find ${locationName} in ${country}`);
    }

    cachedLocations[key] = [roundNumber(result.lat), roundNumber(result.lng)];

    return cachedLocations[key];
  });
}

let cached = [];

try {
  const previous = JSON.parse(fs.readFileSync('./team.json', 'utf8'));
  if (_.get(previous, 'version') === TEAM_MAP_VERSION) {
    cached = previous.team;
  }
} catch (e) {
  console.log('Could not load existing reverse coded team.json');
}

function getMemberPicture(picture) {
  if (picture.startsWith('https://')) {
    return picture;
  }
  const pictureName = picture.replace(/\.(png|jpe?g)$/gi, '');
  return `https://about.gitlab.com/images/team/${pictureName}-crop.jpg`;
}

function compareMembers(a, b) {
  return (
    a.name === b.name &&
    a.locality === b.locality &&
    a.country === b.country &&
    getMemberPicture(b.picture) === getMemberPicture(a.picture)
  );
}

function mapMember(member) {
  if (cached[member.name]) {
    console.log(`Cached result for ${member.name}`);
    return cached[member.name];
  }

  console.log(
    `Searching location for ${member.name}: ${member.locality || 'No locality'} in ${
      member.country
    }`
  );

  return getLocation(member.locality, member.country)
    .then(location => {
      console.log(
        `${member.name}; ${member.locality || 'No locality'}, ${member.country}: ${location}`
      );

      return {
        location: location,
        country: member.country,
        name: member.name,
        locality: member.locality,
        picture: member.picture,
      };
    })
    .catch(e => Promise.reject(new Error(`Could not load location for ${member.name}:\n\t${e}`)));
}

function bail(e) {
  console.warn(`Error creating the team page map:\n\t${e}`);
  process.exit(1);
}

// Get document, or throw exception on error
try {
  const doc = yaml.load(fs.readFileSync('./team.yml', 'utf8'));

  const members = doc
    .filter(
      member =>
        member.type !== 'vacancy' &&
        member.country &&
        member.country !== 'Remote' &&
        member.slug !== 'open-roles' &&
        member.picture !== '../gitlab-logo-extra-whitespace.png'
    )
    .map(member => {
      const locality = getLocality(member.locality);
      const picture = getMemberPicture(member.picture);
      return { ...member, locality, picture };
    })
    .sort(function(a, b) {
      if (a.start_date > b.start_date) {
        return 1;
      }
      if (a.start_date < b.start_date) {
        return -1;
      }
      return 0;
    });

  const tooMuch = _.differenceWith(cached, members, compareMembers);

  if (tooMuch.length > 0) {
    console.log(`Going to remove ${tooMuch.map(x => x.name).join(', ')}`);
    cached = _.difference(cached, tooMuch);
  }

  cached = _.keyBy(cached, 'name');

  Promise.map(members, mapMember, { concurrency: 1 })
    .then(team => {
      console.log('\nFound a location for all team members');
      const result = { version: TEAM_MAP_VERSION, team };
      return fs.writeFileSync('./team.json', JSON.stringify(result));
    })
    .catch(bail);
} catch (e) {
  bail(e);
}
