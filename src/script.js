/*
 * Mapbox and OpenStreetMap (especially the latter!) require proper attribution.
 * More info: https://www.mapbox.com/help/how-attribution-works/
 */
var attribution =
  '<a href="https://www.geonames.org/about.html" target="_blank">&copy; GeoNames</a>' +
  ' <a href="https://www.mapbox.com/about/maps/" target="_blank">&copy; Mapbox</a>' +
  ' <a href="http://www.openstreetmap.org/about/" target="_blank">&copy; OpenStreetMap</a>' +
  ' <b><a href="https://www.mapbox.com/feedback/" target="_blank">Improve this map</a></b>';

function inIframe() {
  try {
    return window.self !== window.top;
  } catch (e) {
    return true;
  }
}

if (inIframe()) {
  attribution += ' | <a href="" target="_blank">Full Screen</a>';
}

var map = L.map('mapid', {
  minZoom: 2,
  maxZoom: 12,
  zoomSnap: 1,
  zoomDelta: 1,
  zoomControl: false,
  attributionControl: false,
  scrollWheelZoom: false,
});

// Access token for mapBox
var accessToken =
  'pk.eyJ1IjoibWFwYm94Z2l0bGFiIiwiYSI6ImNqaTA2Mmh0cDB5dHczcm55YWk3enE2cDEifQ.-WW1euS5jjZ22By2gRDesA';

// The Tile URL
var tileUrl =
  'https://api.mapbox.com/styles/v1/mapbox/dark-v9/tiles/256/{z}/{x}/{y}{r}?access_token={accessToken}';

var tileLayer = L.tileLayer(tileUrl, {
  accessToken: accessToken,
  attribution: '',
});

var markerClusterGroup = L.markerClusterGroup({
  spiderfyOnMaxZoom: false,
  showCoverageOnHover: false,
  zoomToBoundsOnClick: false,
  elementsPlacementStrategy: 'one-circle',
  spiderfyDistanceMultiplier: 1.8,
  iconCreateFunction: function(cluster) {
    var markers = cluster.getAllChildMarkers();

    var imgs = markers.slice(0, 4).map(function(marker) {
      return '<img src="' + marker.$member.picture + '"/>';
    });

    var count = '<div class="count">' + markers.length + '</div>';

    return L.divIcon({
      html: '<div class="group-' + imgs.length + '">' + imgs.join('\n') + count + '</div>',
      iconSize: [50, 50],
    });
  },
});

function allInOnePlace(markers) {
  return (
    markers
      .map(function(marker) {
        return marker.$member.checksum;
      })
      .filter(function(e, i, arr) {
        return arr.lastIndexOf(e) === i;
      }).length < 3
  );
}

markerClusterGroup.on('clusterclick', function(a) {
  var markers = a.layer.getAllChildMarkers();

  // We want to zoom in if our zoom level is to low or if we have more then 10 people
  // We do not want to zoom in, if we have all people in one place (e.g. San Francisco)
  if ((a.layer._zoom < 6 || markers.length > 10) && !allInOnePlace(markers)) {
    a.layer.zoomToBounds({ padding: [20, 20] });
  } else {
    map.panTo(a.layer.getBounds().getCenter());
    a.layer.spiderfy();
  }
});

var registeredHandler = false;

function registerScrollDisableHandler() {
  function disable() {
    map.scrollWheelZoom.disable();
    map.off('blur');
    map.off('mouseout');
    registeredHandler = false;
    registerScrollEnableHandler();
  }

  map.on('blur', disable);

  map.on('mouseout', disable);
}

function registerScrollEnableHandler() {
  if (registeredHandler) {
    return;
  }

  registeredHandler = true;

  map.on('click', function() {
    map.scrollWheelZoom.enable();
    map.off('click');
    registerScrollDisableHandler();
  });
}

function initializeMap(members) {
  var markerArray = [];

  var bounds = [];

  members.forEach(function(member) {
    var memberIcon = L.icon({
      iconUrl: member.picture,
      iconSize: [50, 50],
      popupAnchor: [0, -25],
    });

    var marker = L.marker(member.location, { icon: memberIcon });

    bounds.push(member.location);

    marker.$member = member;
    marker.$member.checksum = member.location[0] + member.location[1];

    var name = '<div class="member-name">' + member.name + '</div>';
    var location = member.locality ? member.locality + ' &middot; ' : '';
    location = '<div class="member-location">' + location + member.country + '</div>';

    marker.bindPopup(name + location);

    markerArray.push(marker);
  });

  markerClusterGroup.addLayers(markerArray);

  map.fitBounds(bounds);

  document.querySelector('#status').remove();

  map.addLayer(tileLayer);

  var attributionControl = L.control.attribution();
  attributionControl.addAttribution(attribution);
  map.addControl(L.control.zoom());
  map.addControl(attributionControl);

  map.addLayer(markerClusterGroup);

  registerScrollEnableHandler();
}

var xhr = new XMLHttpRequest();
xhr.onreadystatechange = function() {
  try {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      var data = JSON.parse(xhr.responseText);
      return initializeMap(data.team);
    }
  } catch (e) {
    console.log('An Error happened loading the map', e);
    document.querySelector('#status').innerHTML =
      '<span>An error happened while loading the map.' +
      ' Please try again.' +
      '<br/>' +
      'If this error persists, please ' +
      '<a href="https://gitlab.com/gitlab-com/teampage-map/issues" target="_blank">file an issue</a>.</span>';
  }
};
xhr.open('GET', './team.json', true);
xhr.send(null);
